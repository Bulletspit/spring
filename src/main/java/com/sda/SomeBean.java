package com.sda;

import lombok.ToString;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
@ToString
public class SomeBean {
    private String title = "";
    int age;
    public List<String> list;
    public Set<String>set;
    public String[]table;
    public Map<Integer, String> map;

    public SomeBean(String title, int age, List<String> list, Set<String> set, String[] table, Map<Integer, String> map) {
        this.title = title;
        this.age = age;
        this.list = list;
        this.set = set;
        this.table = table;
        this.map = map;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public Set<String> getSet() {
        return set;
    }

    public void setSet(Set<String> set) {
        this.set = set;
    }

    public String[] getTable() {
        return table;
    }

    public void setTable(String[] table) {
        this.table = table;
    }

    public Map<Integer, String> getMap() {
        return map;
    }

    public void setMap(Map<Integer, String> map) {
        this.map = map;
    }

}
