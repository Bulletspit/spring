package com.sda;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("application-context.xml");

//        SomeBean test1 = context.getBean("test", SomeBean.class);
//
//        SomeBean2 somebean2v2 = context.getBean("someBean2v2", SomeBean2.class);
//        SomeBean2 somebean2v3 = context.getBean("someBean2v3", SomeBean2.class);
//        System.out.println(test1);
//        System.out.println(somebean2v2);
//        System.out.println(somebean2v3);
//        LocalDateTime ldt = LocalDateTime.now();
//        LocalDateTime ldt2 = context.getBean("ldt2",LocalDateTime.class);
//        System.out.println(ldt2);

        SomeBean3 someBean3 =context.getBean("someBean3",SomeBean3.class);
        System.out.println(someBean3);

    }
}
