package com.sda;

import lombok.ToString;

@ToString
public class SomeBean2 {
    private SomeBean someBean;

    public SomeBean2(SomeBean someBean) {
        this.someBean = someBean;
    }

    public SomeBean getSomeBean() {
        return someBean;
    }

    public void setSomeBean(SomeBean someBean) {
        this.someBean = someBean;
    }


}
